import React from 'react';
import './App.css';
import FirstNames from './Components/FirstNames.js';
import LastNames from './Components/LastNames';

function App() {


    let dataFirst = ['Jane','John','Joe','Will','Jesse']
    let dataLast = ['Doe','Scott','Soap','Shakespeare','James']

    
    return (
      <div className="App">
        <header className="App-header">

        {
          dataFirst.map((item,idx)=>{

            return (
              <div>
                  <FirstNames dataFirst = {item}/>
                  <LastNames dataLast = {dataLast[idx]}/>
              </div>
            )
          })
        }
        
        </header>
      </div>
    );
}

export default App;